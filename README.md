# Лабораторна робота №6. Масиви
# 1 ВИМОГИ
## 1.1 Розробник
Бондар Артем 
КIТ - 121 Б
## 1.2 Загальне завдання
Індивідуальні завдання.
## 1.3 Задача
```
23. (**) Перетворити число (максимальне значення якого - 9999) в рядок. (усі символи
нижнього регістру на виході). Наприклад,
• 123 – “one hundred twenty three”,
• 4311 – “four thousands three hundreds eleven”.
```
# 2 ОПИС ПРОГРАМИ
## 2.1 Функціональне призначення
Перетворити число (максимальне значення якого - 9999) в рядок.
## 2.2 Опис логічної структури
Ми беремо та розкладаємо число по його тисячах, сотнях, десятках та единицях, а потiм записуемо це в рядок. 
## 2.3 Важливі фрагменти програми
```
char * Digit_To_Str(int digit){
    if(9 >= digit & digit >= 0){
        return "Eror";
    }
    switch(digit){
        case 0:
        return "zero";
        break;
        case 1:
        return "one";
        break;
        case 2:
        return "two";
        break;
        case 3:
        return "tree";
        break;
        case 4:
        return "four";
        break;
        case 5:
        return "five";
        break;
        case 6:
        return "six";
        break;
        case 7:
        return "seven";
        break;
        case 8:
        return "eight";
        break;
        case 9:
        return "nine";
        break;
        default:
        return "Eror";
        break;
    }
}
char * Remainder_Digit_To_Str(int digit){
    if(19 >= digit & digit >= 10){
        return "Eror";
    }
    switch(digit){
        case 10:
        return "ten";
        break;
        case 11:
        return "eleven";
        break;
        case 12:
        return "twelve";
        break;
        case 13:
        return "thirteen";
        break;
        case 14:
        return "fourteen";
        break;
        case 15:
        return "fiveteen";
        break;
        case 16:
        return "sixteen";
        break;
        case 17:
        return "seventeen";
        break;
        case 18:
        return "eightteen";
        break;
        case 19:
        return "nineteen";
        break;
        default:
        return "Eror";
        break;
    }
}

char * b_Digit_To_Str(int digit){
    if(20 <= digit & digit <= 90){
        return "Eror";
    }
    switch(digit){
        case 20:
        return "twenty";
        break;
        case 30:
        return "thirty";
        break;
        case 40:
        return "forty";
        break;
        case 50:
        return "fifty";
        break;
        case 60:
        return "sixty";
        break;
        case 70:
        return "seventy";
        break;
        case 80:
        return "eighty";
        break;
        case 90:
        return "ninety";
        break;
        default:
        return "Eror";
        break;
    }
}

const char * Num_To_Str(int num){
    char* res = "";
    if(num <= 9999 && num >= 0){
        int tmp_thousand = num / 1000;
        if(tmp_thousand > 0){
            res = strcat(res, Digit_To_Str(tmp_thousand));
            res = strcat(res, " thousand ");
        }
        int tmp_hundred = num / 100 - tmp_thousand;
        if(tmp_hundred > 0){
            res = strcat(res,Digit_To_Str(tmp_hundred));
            res = strcat(res, " hundred ");
        }
        int tmp_remainder = num % 10; 
        int tmp_dozens = num / 10;
        if(tmp_dozens < 19 & tmp_dozens > 9){
            res = strcat(res,Remainder_Digit_To_Str(tmp_dozens));
            return res;
        }
        tmp_dozens -= tmp_remainder;
        if(tmp_dozens >= 20 && tmp_dozens <= 90){
            res = strcat(res,b_Digit_To_Str(tmp_dozens));
        }
        res = strcat(res,Digit_To_Str(tmp_remainder));
        return res;
    }
    else{
        return "Error";
    }
}
```
# 3 ВАРІАНТИ ВИКОРИСТАННЯ
# ВИСНОВКИ
